import pandas as pd

#consultar el numero de filas que puede mostrar pandas de un dataframe.
AVISO = pd.get_option("display.max_rows")
print(AVISO)
#editar el numero max de filas que puede mostrar pandas.
pd.set_option("display.max_rows", 300)
AVISO = pd.get_option("display.max_rows")
print(AVISO)
#restablecer valores predeterminados de las filas a mostrar de un data frame de pandas
pd.reset_option("display.max_rows")
AVISO = pd.get_option("display.max_rows")
print(AVISO)