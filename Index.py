from pprint import pprint as nprint
from nbformat import read
import pandas as pd
import openpyxl as op
import os
import sys
sys.path.append(".")
from servicios.Censos import * #importacion de archivo con las funciones de cada Censo

#VARIABLES GLOBALES
ruta_p = os.getcwd() #Directorio de archivos raiz
CNPJF = "\\excel\\CNGF_2021_M4_S2_CFE.xlsx"
#lectura de libro excel con Openpyxl
libro_op = op.load_workbook(ruta_p + CNPJF)
nom_hojas =libro_op.sheetnames #lista de nombres de cada hoja del libro

#lectura de libro excel con PANDAS *opcional usar read.excel o ExcelFile.
libro_pd = pd.ExcelFile(ruta_p + CNPJF, engine='openpyxl')

#Enviamos el libro de excel a la funcion correspondiente al censo seleccionado
resultado = CNGF_M4(libro_pd,libro_op,nom_hojas)