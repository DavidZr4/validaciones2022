import pandas as pd
import openpyxl as op
import os
import sys
sys.path.append(".")
from servicios.metodos_ppal import  *
from servicios.funciones_ppal import *

'''DEFINIMOS CADA UNO DE LOS CENSOS DE GOBIERNO PARA ESTABLECER 
 UN FLUJO ESPECIFICO Y PROCEDER A REALIZAR LA VALIDACION DE C/U'''

#VARIABLES GLOBALES, INSTANCIA DE CLASES, etc.
OpBasicas = OperacionesBasicas() #Variable creada apartir de la clase de opaciones basicas del archivo de metodos_ppal


#CENSO NACIONAL DE GOBIERNO FEDERAL (MODULO 4)
def CNGF_M4(libro_pd,libro_op,nom_hojas):
    
    hoja = OpBasicas.ObtenerHojas(nom_hojas)#Lista ordenada con los nombres las hojas del libro
    
    #SECCION1
    Secc1 = pd.read_excel(libro_op, sheet_name=hoja[0] , engine='openpyxl')#Hoja 1, *hoja[numero de hoja segun elementos de variable "hoja"]
    lsPreguntas = OpBasicas.ObtenerPreguntas(Secc1)#Lista ordenada con los numeros de pregunta de la hoja.        
    resultado = CNGF_M4_VAL.validar_Secc1(Secc1, lsPreguntas)#Resultado final de validaciones en la hoja.
    
    #SECCION 2
    Secc2 = pd.read_excel(libro_op, sheet_name=hoja[1] , engine='openpyxl')#hoja 2 - DataFrame
    resultado = CNGF_M4_VAL.validar_Secc2(Secc2)
    
    return


#CENSO NACIONAL DE SISTEMA PENITENCIARIO FEDERAL (MODULO 2)
def CNSIPEF_M1(libro):
    return

#CENSO NACIONAL DE DERECHOS HUMANOS FEDERAL (MODULO 2)
def CNDHF_M1(libro):
    return