import pandas as pd
import openpyxl as op
from pprint import pprint as nprint
import os
import sys
sys.path.append(".")
from servicios.metodos_ppal import  *

''' AQUI SE ENCUENTRAN TODAS LA FUNCIONES PRINCIPALES 
    PARA LA GENERACION DE VALIDACIONES Y OBTENCION DE 
    DATOS'''
    
#VARIABLES GLOBALES, INSTANCIA DE CLASES, etc.
OpBasicas = OperacionesBasicas() #Variable creada apartir de la clase de opaciones basicas del archivo de metodos_ppal

#CENSO NACIONAL DE GONIERNO FEDERAL (Modulo 4)
class CNGF_M4_VAL():

    #Validaciones correspondientes para la Seccion 1
    def validar_Secc1(hoja, lsPreguntas):
        #PREGUNTA 1:
        #.
        #.
        #.
        #PREGUNTA 7: Validaciones - Sumatoria de columnas en tabla dividida en dos partes        
         
        #PREGUNTA 8: Validaciones - Sumatoria de columnas en tabla dividida en dos partes     
        totales1 = OpBasicas.Sumatoria_Columnas_continuas(278, 286, 19, 30, hoja)#Saca los totales de la primera parte de la tabla
        totales2 = OpBasicas.Sumatoria_Columnas_continuas(293, 301, 19, 30, hoja)#Saca los totales de la segunda parte de la tabla
           
        return
    #Validaciones correspondientes para la Seccion 2
    def validar_Secc2(hoja):    
        return
    #Validaciones correspondientes para la Seccion 3
    def validar_Secc3(hoja):                
        return
    
    