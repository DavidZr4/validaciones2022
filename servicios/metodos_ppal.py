import pandas as pd
import openpyxl as op
from pprint import pprint as nprint
import sys
sys.path.append(".")

''' AQUI SE ENCUENTRAN TODOS LOS METODOS PRINCIPALES
    PARA DISTINTOS PROCESOS BASICOS ANTES DE REALIZAR
    LAS VALIDACIONES.'''
#VARIABLES GLOBALES

class OperacionesBasicas:

    #METODO PARA OBTENER LAS HOJAS DEL EXCEL QUE CONTENGAN LA PALABRA "Secc"
    @staticmethod
    def ObtenerHojas(hojaspd):        
        hojas_secc = []
        
        for x in hojaspd:
            if "Secc" in x:
                hojas_secc.append(x)
        return(hojas_secc)
    
    #METODO PARA OBTENER EL NUMERO DE INICIO DE CADA PREGUNTA SEGUN LA HOJA DEL EXCEL DONDE SE ESTE TRABAJANDO.
    @staticmethod
    def ObtenerPreguntas(hoja):
        lsPreguntas = []#variable donde se obtiene el numero de fila donde empieza cada pregunta de la hoja seleccionada y lo guarda en una lista ordenada.
        contador = 0 #variable que nos ayuda a determinar el numero de fila donde esta iterando el ciclo
                  
        #1.- OBTENEMOS LOS DATOS DE LA PRIMER COLUMNA DONDE SE ENCUENTRAN LOS NUMEROS DE CADA PREGUNTA DE LA HOJA SELECCIONADA    
        for i in hoja["Unnamed: 0"]: 
            contador +=1
            if pd.isna(i) == False:
                lsPreguntas.append(contador)    
        return(lsPreguntas)

    """ #ESTA FUNCION RECIBE LAS FILAS Y COLUMNAS DE UNA SECCION ESPECIFICA DE LA HOJA 
    #PARA REALIZAR LA SUMA CONSECUTIVA DE LAS COLUMNAS DE UNA TABLA
    @staticmethod
    def Sumatoria_Columnas_continuas(fila_inicio, fila_fin, col_inicio, col_fin, hoja):
        #Condiciones base para crear una relacion exacta entre las coordenadas de pandas y openpyxl con las coordenadas del excel
        fila_inicio, fila_fin, col_inicio += -1
        
        #Variables a utiliar
        contador = 0
        Suma_Col = []
        datos = hoja.iloc[fila_inicio:fila_fin, col_inicio:col_fin]#Data frame de la sección especifica seleccionada
        nprint(datos)
         """
             
    